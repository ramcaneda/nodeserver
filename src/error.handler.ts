import { Request, Response, NextFunction, Router, Errback } from "express";

export default function ErrorHandler(err: any, req: Request, res: Response, next: NextFunction){
  let timestamp = (new Date()).getTime();
  let error = {
    timestamp,
    code: err.code,
    message: (err.errors)?err.errors.map((i: any)=>i.message): err.message
  };

  let request = {
    method: req.method,
    url: req.originalUrl,
    headers: req.headers,
    body: req.body 
  }

  let response = {
    error
  }
  console.error(timestamp, {
    request,
    response
  });

  res.status(err.status || (err.name.includes("Sequelize")?400:500))
    .send({
      status: "Failed",
      error
    });
}