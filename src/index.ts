import * as https from 'https';
import * as http from 'http';
import * as fs from 'fs';
import express from "express";
import app from './app';
const key = fs.readFileSync('sslcert/key.key', 'utf8');
const cert = fs.readFileSync('sslcert/cert.crt', 'utf8');

const args = require('minimist')(process.argv);
const version = args.version || process.env.APP_VERSION || 'v1.0a';
var useHttps = false;
if(args.https != undefined){
  useHttps = args.https != 'false';
}
const port = args.port || process.env.PORT || ((useHttps)?443:80);
const expressApp = express().use(`/api/${version}`, app);
var server;
if(useHttps){
  server = https.createServer({key, cert}, expressApp);
}else{
  server = http.createServer(expressApp);
}

server.listen(port, () => {
  console.log(`SERVER ${version} STARTED ====`);
  console.log(`PORT: ${port}`);
  console.log(`HTTPS: ${useHttps}`);
});