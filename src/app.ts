import { Request, Response, NextFunction, Router } from "express";
import express from "express";
import bodyParser from 'body-parser';
import helloModule from './hello.module';
import * as https from 'https';
import ErrorHandler from "./error.handler";
import httpError from "./types/errors/httpError";
import notificationService from "./notification.service";

var app = express();
app.use(bodyParser.json({strict: false}));
app.use((req: Request, res: Response, next: NextFunction) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/testing', (req: Request, res: Response) => {
  res.send(new Date());
});

app.get('/docrash', (req: Request, res: Response, next: NextFunction) => {
  next(new Error('we crashed'));
});

app.use('/register', (req: Request, res: Response, next: NextFunction)=>{
  try{
    let token = req.body.token || req.query.token;
    let id = notificationService.register(token);
    res.send({
      id
    });
  }catch(e){
    next(e);
  }
});

app.get('/sendnotif', (req: Request, res: Response, next: NextFunction)=>{
  try{
    let msgToSend = req.query.msg || 'PING';
    let userId = parseInt(req.query.userId);
    
    notificationService.sendNotif(userId, msgToSend);
    res.send('ok');
  }catch(e){
    next(e);
  }
});

app.use((req: Request, res: Response, next: NextFunction) => {
  next(new httpError(404, `'${req.originalUrl}' not found`));
});

app.use(helloModule);

app.use(ErrorHandler);

export default app;