import { Request, Response, NextFunction, Router } from "express";

function HelloHandle(req: Request, res: Response, next: NextFunction){
  res.send('hello world');
}

export default Router().get('/', HelloHandle);