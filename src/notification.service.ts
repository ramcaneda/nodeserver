const https = require('https');
const config = {
  "projectId": "tsekapp-a0e650",
  "webAPIKey": "AAAA2XUJzfM:APA91bHgzRAqniPTwDO33GBd5Jlj-XxPGWeen7aagLflOUGKL9P5yWsfR7wf4DSEGoJyPPvumbVPokhA-t9OZCvh9iMdVCdo1drM4lAa9RRkQUpN6uztr81-bzKIzVKYiSLa5p1Dje74"
}

class NotifService {
  users: any[]= [];
  register(token: string){
    let id = this.users.push({token});
    return id - 1;
  }

  sendNotif(id: number, message: string, title: string = 'notification', payload = {}){
    let token = this.users[id].token;
    return new Promise((resolve, reject)=>{
      var data = '';
      let request = https.request(
        { host: 'fcm.googleapis.com',
          path: `/fcm/send`,
          method: 'POST',
          headers: { "Authorization": `key=${config.webAPIKey}`, "Content-Type": "application/json" }
        },
        (response: any) => {
          response.on('data', (chunk:any) => {
            data += chunk;
          });
  
          response.on('end', () => {
            let body = JSON.parse(data);
            let res = {
              status: response.statusCode,
              body
            }
            if(body.success == 0){
              reject(body);
            }else{
              resolve(body);
            }
          });
        });
  
  
      request.on('error', (error: any)=>{
        reject(error);
      });
      request.write((new NotificationPayload(title, message, payload, token)).toString());
      request.end();

      }
    );
  }
}

class NotificationPayload {
  registration_ids: string[] = [];

  constructor(public title:string, public body:string, public payload:any, public token:string) {
    this.registration_ids = [];
    if (token) {
      this.registration_ids.push(token);
      this.token = token;
    }
  }

  addTokens(...token: string[]) {
    this.registration_ids.push(...token);
    return this;
  }

  toString() {
    return JSON.stringify({
      notification: {
        title: this.title,
        body: this.body,
        click_action: "FCM_PLUGIN_ACTIVITY",
        icon: "fcm_push_icon",
        sound: "default",
        color: "#3780FF",
        badge: 0
      },
      registration_ids: this.registration_ids,
      data: this.payload
    })
  }
}


export default new NotifService();